import { Component, OnInit, OnDestroy } from '@angular/core';
import { InvestorService, Account, Investor } from '../shared/services/investor.service';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';



@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit, OnDestroy {
  investorId: number;
  investorSubscription: Subscription;
  accounts$ = new Subject();
  pieChartLabels: Label[] = [];
  pieChartData: number[] = [];
  pieChartType: ChartType = 'pie';
  pieChartLegend = true;
  currInvestor: Investor;
  breakdown;
  pieChartColors = [
    {
      backgroundColor: ['#7B1FA2', '#009688', '#FFC107', '#212121'],
    },
  ];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
    },

  };

  constructor(
    private route: ActivatedRoute,
    private investorService: InvestorService,

  ) { }

  ngOnInit() {
    this.route.params.subscribe((data: { id: number }) => {
      this.investorId = data.id;
      this.getAccountByInvestorId(this.investorId);
    });
    this.currInvestor = this.investorService.getCurrInvestor();

  }

  getAccountByInvestorId(id: number) {
    this.investorSubscription = this.investorService.getAccountByInvestorId(id)
      .subscribe((accounts: Array<Account>) => {
        this.accounts$.next(accounts);
        this.parseAccountsData(accounts);
      });
  }

  parseAccountsData(accounts) {
    const holder = {};
    accounts.forEach(account => {
      if (holder.hasOwnProperty(account.type)) {
        holder[account.type] = holder[account.type] + account.amountHeld;
      } else {
        holder[account.type] = account.amountHeld;
      }
    });
    const formattedAccounts = [];

    // tslint:disable-next-line: forin
    for (const prop in holder) {
      formattedAccounts.push({ type: prop, amountHeld: holder[prop] });
    }
    formattedAccounts.forEach(item => {
      this.pieChartData.push(item.amountHeld.toFixed(2));
      this.pieChartLabels.push(item.type);
    });

    this.breakdown = Object.entries(formattedAccounts);
  }

  ngOnDestroy() {
    this.investorSubscription.unsubscribe();
  }

}
