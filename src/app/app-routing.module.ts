import { ReportsComponent } from './reports/reports.component'
import { InvestorDetailsComponent } from './investor-details/investor-details.component'

import { InvestorsComponent } from './investors/investors.component'
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: InvestorsComponent, pathMatch: 'full' },
  { path: 'investor/account/:id', component: InvestorDetailsComponent },
  { path: 'investor/account/report/:id', component: ReportsComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
