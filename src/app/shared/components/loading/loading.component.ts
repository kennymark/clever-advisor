import { Component, OnInit, Input } from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  timeout: boolean;
  @Input() data: any;
  constructor() { }

  ngOnInit() {
    timer(10000).subscribe(x => this.timeout = true);
  }

}
