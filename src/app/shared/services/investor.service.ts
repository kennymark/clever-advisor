import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InvestorService {
  private readonly url = 'https://cleverapih2t2qggn64f7sc4n.azurewebsites.net/api/'
  private currInvestor: Investor;
  constructor(private http: HttpClient) { }

  setCurrInvestor(investor: Investor): void {
    this.currInvestor = investor;
  }

  getCurrInvestor(): Investor {
    return this.currInvestor;
  }


  getAllInvestors(): Observable<Investor[]> {
    return this.http.get<Investor[]>(this.url + 'Investors/');
  }

  getInvestorById(id: number): Observable<Investor> {
    return this.http.get<Investor>(this.url + `Investors/${id}`);
  }

  getInvestorByName(name: string): Observable<Investor[]> {
    const headers = new HttpHeaders({ accept: 'application/json' })
    return this.http.get<Investor[]>(this.url + `Investors/GetBy`, { headers, params: { name } });
  }

  addInvestor(data: Investor) {
    return this.http.post<Investor>(this.url + 'Investors', data);
  }

  getAllAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(this.url + `Accounts`);
  }

  getAccountById(id: number) {
    return this.http.get(this.url + `Accounts/${id}`);
  }

  getAccountByInvestorId(id: number): Observable<Account[]> {
    return this.http.get<Account[]>(this.url + `Accounts/ByInvestorId/${id}`);
  }

  getAccountByAmount(amount: number): Observable<Account> {
    return this.http.get<Account>(this.url + `Accounts/ByAmount/${amount}`);
  }

  updateAccountById(id: number, data: Account) {
    return this.http.put<Account>(this.url + `Accounts/${id}`, data);
  }

  addAccount(data: Account) {
    return this.http.post<Account>(this.url + `Accounts`, data);
  }


}

export interface Investor {
  investorId: number;
  name: string;
  surname: string;
  dateCreated: string;
}

export interface Account {
  accountId?: number;
  investorId: number;
  amountHeld: number;
  type: string;
  nullable?: boolean;
  dateCreated: Date;
}