import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subject, of } from 'rxjs';
import { takeUntil, retry, map, tap, catchError } from 'rxjs/operators';
import { Investor, InvestorService } from './../shared/services/investor.service';
import { NotifierService } from 'angular-notifier';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-investor-details',
  templateUrl: './investor-details.component.html',
  styleUrls: ['./investor-details.component.scss']
})
export class InvestorDetailsComponent implements OnInit {
  investor = new Subject<Investor>();
  accounts = new Subject<Account[]>();

  @ViewChild(DatatableComponent) table: DatatableComponent;
  selected: any[] = [];
  modalRef: BsModalRef;
  investorId: number;
  public columns: { name: string }[] = [
    { name: 'Account ID' },
    { name: 'Investor ID', },
    { name: 'Amount Held', },
    { name: 'Type', },
    { name: 'Date Created' },
  ];

  accountTypes = ['Bond', 'GIA', 'ISA', 'Pension'];
  accountForm: FormGroup;
  temp = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  constructor(
    private investorService: InvestorService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private notifier: NotifierService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.route.params.subscribe((data: { id: number }) => {
      this.investorId = data.id;
      this.getInvestorById(this.investorId);
      this.getAccountByInvestorId(this.investorId);
    });

    this.accountForm = this.fb.group({
      type: ['', Validators.required],
      amountHeld: ['', [Validators.required, Validators.min(1)]]
    });
  }

  getInvestorById(id: number) {
    this.investorService.getInvestorById(id)
      .pipe(takeUntil(this.investor))
      .subscribe((data: Investor) => {
        this.investorService.setCurrInvestor(data);
        this.investor.next(data);
      });
  }

  getAccountByInvestorId(id: number) {
    this.investorService.getAccountByInvestorId(id)
      .pipe(takeUntil(this.accounts))
      .subscribe((data: any) => this.accounts.next(data));
  }

  showAddAccountModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  addNewAccount() {
    const data = {
      accountId: 833,
      investorId: +this.investorId,
      ...this.accountForm.value,
      dateCreated: new Date().toISOString()
    };

    console.log(data);
    this.investorService.addAccount(data)
      .pipe(
        retry(2),
        catchError(err => of(err)),
        tap((res) => {
          if (res.ok) {
            this.notifier.notify('success', 'You are awesome! I mean it!');
          } else {
            this.notifier.notify('danger', 'You are awesome! I mean it!');
          }
          console.log({ res });
        }))
      .subscribe();
    this.modalRef.hide();
  }


}
