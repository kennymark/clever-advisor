import { InvestorService, Investor } from './../shared/services/investor.service'
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent, ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, map, } from 'rxjs/operators';
import { Subject, timer } from 'rxjs'


@Component({
  selector: 'app-investors',
  templateUrl: './investors.component.html',
  styleUrls: ['./investors.component.scss']
})
export class InvestorsComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  selected: any[] = [];
  searchQuery = new Subject();
  timeout = false;

  investors: Investor[];
  public columns: Array<any> = [
    { name: 'Investor ID' },
    { name: 'Name', },
    { name: 'Surname', },
    { name: 'Date Created', },
  ];
  temp = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  constructor(
    private investorService: InvestorService,
    private router: Router
  ) { }

  ngOnInit() {
    this.investorService.getAllInvestors().subscribe(data => {
      this.investors = data;
    });

    this.searchQuery.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      map(term => this.searchByName(term)),
    ).subscribe();
  }

  onCellClick(data) {
    const investor = data.selected[0];
    this.router.navigate(['investor', 'account', investor.investorId]);
  }

  searchByName(val) {
    const nameCapitalized = val.charAt(0).toUpperCase() + val.slice(1);
    this.investorService.getInvestorByName(nameCapitalized)
      .subscribe(data => {
        this.investors = data;
      });
  }
}
