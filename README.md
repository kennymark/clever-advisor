# CleverAdvisor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Issues

*Require 4*
I am confused as to why adding a new account requires using the put method. The only available put method for account is for updating the accounts. 
I try using the `post` method but I always seem to be getting either an error with code `500` or `405`.

Here is a screenshot of the payload
![Screenshot](screenshots/new_account_405.jpg)

*Require 3*

I didn't fully understand what a drill down support from the investors page to the details meant but I interpreted it as just creating a link between the two pages. You can access the details by just clicking on the row itself



## Screenshots

![Screenshot](screenshots/home.png)
![Screenshot](screenshots/investor.png)
![Screenshot](screenshots/reports.png)


